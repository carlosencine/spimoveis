
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/*Import das dependencias do projeto*/
/*import Vuetify from 'vuetify';
Vue.use(Vuetify);*/

import axios from 'axios';
import VueGoodTable from 'vue-good-table';
import VueClip from 'vue-clip';
import VueImg from 'v-img';
import popover from 'vue-js-popover';
import inputmask from 'v-mask';
import vmoney from 'v-money';
import pt_BR from 'vee-validate/dist/locale/pt_BR';
import VeeValidate from 'vee-validate';
import moment from 'moment';
//import VueToastr2 from 'vue-toastr-2';
//import 'vue-toastr-2/dist/vue-toastr-2.min.css'




const config = {
    delay: 0,
    locale: "pt_BR",
};
const money = {
    decimal: ',',
    thousands: '.',
    precision: 2,
    prefix: 'R$ ',
    masked: true
};


Vue.use(VueGoodTable);
Vue.use(VueClip);
Vue.use(VueImg);
Vue.use(popover);
Vue.use(inputmask);
Vue.use(vmoney, money);
Vue.use(VeeValidate, config);
Vue.use(moment);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
axios.defaults.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('admin', require('./components/admin.vue'));
Vue.component('categorias', require('./components/subtipos.vue'));
Vue.component('imoveis', require('./components/imovel.vue'));
Vue.component('addimovel', require('./components/addimovel.vue'));
Vue.component('paginas', require('./components/pagination.vue'));
Vue.component('enterprises', require('./components/empreendimentos.vue'));
Vue.component('proprietarios', require('./components/proprietarios.vue'));
Vue.component('addproprietario', require('./components/addproprietario.vue'));
Vue.component('corretores', require('./components/corretores.vue'));
Vue.component('usuarios', require('./components/usuarios.vue'));
Vue.component('google-map', require('./components/map.vue'));
Vue.component("edit-imovel", require('./components/editimovel.vue'));
Vue.component("show-imovel", require('./components/showimovel.vue'));
Vue.component("edit-proprietarios", require('./components/editproprietario.vue'));
Vue.component("show-proprietario", require('./components/showproprietario.vue'));

const app = new Vue({
    el: '#app'
});
