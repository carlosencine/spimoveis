@extends('adminlte::page')

@section('title', 'Exibir Imóvel')

@section('content')
    <show-imovel></show-imovel>
@stop

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" />--}}
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
@stop

@section('js')
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCkiCUkyB2NtYc1Z9hCYcBAytcvzLQa-Pw&amp;"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>--}}
    <script src="{{asset('js/app.js')}}"></script>
@stop