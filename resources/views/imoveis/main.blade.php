@extends('adminlte::page')

@section('title', 'Cadastro de Imóveis')

@section('content')
    <imoveis></imoveis>
@stop

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('js')
    <script src="{{asset('js/app.js')}}"></script>
@stop