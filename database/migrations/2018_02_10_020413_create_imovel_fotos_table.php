<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImovelFotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imovel_fotos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('imovel_id')->unsigned()->nullable();
			$table->integer('order')->nullable();
			$table->text('path', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('filename')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imovel_fotos');
	}

}
