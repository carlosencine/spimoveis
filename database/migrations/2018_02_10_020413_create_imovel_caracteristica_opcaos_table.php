<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImovelCaracteristicaOpcaosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imovel_caracteristica_opcaos', function(Blueprint $table)
		{
			$table->integer('ID', true);
			$table->string('NOME', 50)->nullable();
			$table->string('REFERENTE', 30)->nullable();
			$table->char('TIPOCASA', 3)->nullable();
			$table->char('TIPOAPARTAMENTO', 3)->nullable();
			$table->char('TIPORURAL', 3)->nullable();
			$table->char('TIPOFLAT', 3)->nullable();
			$table->char('TIPOCOMERCIAL', 3)->nullable();
			$table->char('TIPOTERRENO', 3)->nullable();
			$table->char('INATIVO', 3)->nullable();
			$table->string('URL')->nullable();
			$table->char('ATRIBUIRDESCRICAO', 3)->nullable();
			$table->char('TIPOBOX', 3)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imovel_caracteristica_opcaos');
	}

}
