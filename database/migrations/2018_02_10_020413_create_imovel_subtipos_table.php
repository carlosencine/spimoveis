<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImovelSubtiposTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imovel_subtipos', function(Blueprint $table)
		{
			$table->increments('ID');
			$table->string('TIPO', 15)->nullable();
			$table->string('SUBTIPO', 30)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imovel_subtipos');
	}

}
