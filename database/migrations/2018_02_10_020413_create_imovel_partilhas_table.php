<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImovelPartilhasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imovel_partilhas', function(Blueprint $table)
		{
			$table->integer('ID', true);
			$table->integer('IMOVEL_ID')->nullable()->index('imovel_partilhas_imovel_id_foreign');
			$table->integer('PROPRIETARIO_ID')->nullable()->index('imovel_partilhas_proprietario_id_foreign');
			$table->float('PORCENTAGEM', 10, 0)->nullable();
			$table->float('TAXAADMINISTRACAO', 10, 0)->nullable();
			$table->float('TAXAADMINISTRACAOVALOR', 10, 0)->nullable();
			$table->string('MODOPAGAMENTO', 30)->nullable();
			$table->integer('REDE_PARCEIRO_REGISTRO_ID')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imovel_partilhas');
	}

}
