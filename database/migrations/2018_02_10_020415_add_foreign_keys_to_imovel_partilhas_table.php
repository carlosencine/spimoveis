<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToImovelPartilhasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('imovel_partilhas', function(Blueprint $table)
		{
			$table->foreign('IMOVEL_ID')->references('ID')->on('imovels')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('PROPRIETARIO_ID')->references('ID')->on('proprietarios')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('imovel_partilhas', function(Blueprint $table)
		{
			$table->dropForeign('imovel_partilhas_imovel_id_foreign');
			$table->dropForeign('imovel_partilhas_proprietario_id_foreign');
		});
	}

}
