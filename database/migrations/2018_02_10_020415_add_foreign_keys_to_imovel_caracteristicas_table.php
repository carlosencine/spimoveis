<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToImovelCaracteristicasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('imovel_caracteristicas', function(Blueprint $table)
		{
			$table->foreign('CARACTERISTICA_OPCAO_ID', 'fk_movel_opcaos')->references('ID')->on('imovel_caracteristica_opcaos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('IMOVEL_ID')->references('ID')->on('imovels')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('imovel_caracteristicas', function(Blueprint $table)
		{
			$table->dropForeign('fk_movel_opcaos');
			$table->dropForeign('imovel_caracteristicas_imovel_id_foreign');
		});
	}

}
