<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImovelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imovels', function(Blueprint $table)
		{
			$table->integer('ID', true);
			$table->integer('OUTRO_ID')->unsigned()->nullable();
			$table->integer('EMPREENDIMENTO_ID')->nullable()->index('fk_empreendimentos_imovel');
			$table->integer('IMOVEL_SUBTIPO_ID')->unsigned()->nullable()->index('imovels_imovel_subtipo_id_foreign');
			$table->integer('CODIGO')->unsigned()->nullable()->index('CODIGO');
			$table->string('CODIGOANUNCIO', 150)->nullable();
			$table->date('CHAVESENTREGA')->nullable();
			$table->char('INATIVO', 3)->nullable();
			$table->char('OPCAOSF', 3)->nullable();
			$table->char('OCUPADO', 3)->nullable();
			$table->string('TIPOGARANTIA', 50)->nullable();
			$table->char('FIADOR', 3)->nullable();
			$table->char('DEPOSITO', 3)->nullable();
			$table->char('PLACA', 3)->nullable();
			$table->timestamp('DATACAPTACAO')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('CHAVESQUADRO', 15)->nullable();
			$table->string('TIPOENDERECO', 10)->nullable();
			$table->string('ENDERECO', 50)->nullable();
			$table->string('NUMERO', 10)->nullable();
			$table->string('COMPLEMENTO', 50)->nullable();
			$table->text('REFERENCIA', 65535)->nullable();
			$table->string('CEP', 9)->nullable();
			$table->string('BAIRRO', 50)->nullable();
			$table->string('CIDADE', 50)->nullable();
			$table->string('UF', 40)->nullable();
			$table->string('GUIA', 15)->nullable();
			$table->string('TIPO', 15)->nullable();
			$table->string('RELOGIOLUZ', 30)->nullable();
			$table->string('RELOGIOAGUA', 30)->nullable();
			$table->string('RELOGIOGAS')->nullable();
			$table->string('CODCLIENTELUZ')->nullable();
			$table->string('CODCLIENTEAGUA')->nullable();
			$table->string('CODCLIENTEGAS')->nullable();
			$table->string('CODCLIENTEIPTU')->nullable();
			$table->string('QUANTIDADECONTROLES')->nullable();
			$table->char('VISITA', 3)->nullable();
			$table->integer('DORMITORIO')->nullable();
			$table->integer('BANHEIRO')->nullable();
			$table->integer('SUITE')->nullable();
			$table->string('ENERGIASEPARADA', 5)->nullable();
			$table->string('AGUASEPARADA', 5)->nullable();
			$table->string('CONDOMINIOFECHADO', 5)->nullable();
			$table->string('CAMERASEGURANCA', 5)->nullable();
			$table->string('ENTRADAINDEPENDENTE', 5)->nullable();
			$table->string('CERCAELETRICA', 5)->nullable();
			$table->float('TERRENOLADOESQUERDO', 10, 0)->nullable();
			$table->float('TERRENOLADODIREITO', 10, 0)->nullable();
			$table->float('CONSTRUIDA', 10, 0)->nullable();
			$table->integer('VAGA')->nullable();
			$table->string('IPTU', 30)->nullable();
			$table->string('MATRICULA', 30)->nullable();
			$table->float('VALORVENDA', 10)->nullable();
			$table->float('VALORLOCACAO', 10)->nullable();
			$table->float('VALORCONDOMINIO', 10)->nullable();
			$table->float('HONORARIOSVENDA', 10)->nullable();
			$table->float('HONORARIOSLOCACAO', 10)->nullable();
			$table->char('VENDA', 3)->nullable();
			$table->char('LOCACAO', 3)->nullable();
			$table->float('TERRENOTOTALM2', 10, 0)->nullable();
			$table->string('STATUS', 10)->nullable();
			$table->string('HORARIOVISITA', 30)->nullable();
			$table->string('DESCRICAO')->nullable();
			$table->char('CONDOMINIOAGUA', 3)->nullable();
			$table->char('CONDOMINIOLUZ', 3)->nullable();
			$table->char('CONDOMINIOGAS', 3)->nullable();
			$table->char('CONDOMINIOTVCABO', 3)->nullable();
			$table->char('CONDOMINIOIPTU', 3)->nullable();
			$table->float('TERRENOFRENTE', 10, 0)->nullable();
			$table->float('TERRENOFUNDOS', 10, 0)->nullable();
			$table->float('TERRENORAIO', 10, 0)->nullable();
			$table->string('TIPOAREA', 20)->nullable();
			$table->string('LATITUDE', 30)->nullable();
			$table->string('LONGITUDE', 30)->nullable();
			$table->integer('ALTITUDE')->nullable();
			$table->date('DATAFINALCONTRATO')->nullable();
			$table->float('VALORLOCACAOM2', 10, 0)->nullable();
			$table->float('VALORVENDAM2', 10, 0)->nullable();
			$table->char('EXCLUSIVIDADE', 3)->nullable();
			$table->string('FRENTEPARA', 50)->nullable();
			$table->string('FUNDOSPARA', 50)->nullable();
			$table->string('ESQUERDAPARA', 50)->nullable();
			$table->string('DIREITAPARA', 50)->nullable();
			$table->date('CARTORIODATAREGISTRO')->nullable();
			$table->integer('CARTORIOFOLHA')->nullable();
			$table->integer('CARTORIOREGISTRO')->nullable();
			$table->string('CARTORIOLIVRO', 10)->nullable();
			$table->string('CARTORIO', 50)->nullable();
			$table->string('TIPOZONA', 10)->nullable();
			$table->integer('DESTAQUE')->nullable();
			$table->integer('COMPLEMENTOANDAR')->nullable();
			$table->integer('COMPLEMENTOAPTO')->nullable();
			$table->text('DESCRICAOSITE')->nullable();
			$table->char('LANCAMENTO', 3)->nullable();
			$table->char('TEMPORADA', 3)->nullable();
			$table->float('VALORIPTU', 10)->nullable();
			$table->char('DIVULGARVALOR', 3)->nullable();
			$table->char('ALTOPADRAO', 3)->nullable();
			$table->integer('PARAMETRIZACAO_ID')->nullable();
			$table->float('VALORVENDACUSTO', 10, 0)->nullable();
			$table->float('AREAUTIL', 10, 0)->nullable();
			$table->char('SITEDIVULGAR', 3)->nullable();
			$table->integer('SITE_AVALIACAO')->nullable();
			$table->integer('SITE_AVALIACAO_QUANTIDADE')->nullable();
			$table->integer('SITE_VISITAS')->nullable();
			$table->dateTime('CREATED_AT');
			$table->dateTime('UPDATED_AT');
			$table->dateTime('DATAGRAVACAO');
			$table->string('PAIS', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imovels');
	}

}
