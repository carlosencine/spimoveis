<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmpreendimentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empreendimentos', function(Blueprint $table)
		{
			$table->integer('ID', true);
			$table->string('NOME', 50)->nullable();
			$table->string('ZELADOR', 50)->nullable();
			$table->string('PORTEIRO', 50)->nullable();
			$table->string('TELEFONE1', 30)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empreendimentos');
	}

}
