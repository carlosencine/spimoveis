<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 190);
			$table->string('email', 190)->unique();
			$table->string('password', 190);
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->char('ADMINISTRADOR', 3)->nullable();
			$table->char('ADMINISTRADORLIBERARFUNCAO', 3)->nullable();
			$table->char('RELATORIOCRIARALTERAR', 3)->nullable();
			$table->string('TELEFONE', 20)->nullable();
			$table->char('INATIVO', 3)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
