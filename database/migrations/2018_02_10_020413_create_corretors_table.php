<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCorretorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('corretors', function(Blueprint $table)
		{
			$table->integer('ID', true);
			$table->char('INATIVO', 3)->nullable();
			$table->char('TFJ', 1)->nullable();
			$table->string('NOMERAZAO', 50)->nullable();
			$table->integer('CODIGO')->nullable();
			$table->string('APELIDOFANTASIA', 50)->nullable();
			$table->string('CPFCNPJ', 50)->nullable();
			$table->string('RGIE', 50)->nullable();
			$table->string('INSCMUNICIPAL', 50)->nullable();
			$table->char('SEXO', 1)->nullable();
			$table->date('DATANASCIMENTO')->nullable();
			$table->string('NATURALIDADE', 50)->nullable();
			$table->string('NACIONALIDADE', 50)->nullable();
			$table->string('ESTADOCIVIL', 50)->nullable();
			$table->string('CONTACORRENTE', 30)->nullable();
			$table->char('TIPOCONTA', 1)->nullable();
			$table->integer('CODBANCOCORRENTE')->nullable();
			$table->string('BANCOCORRENTE', 30)->nullable();
			$table->string('ENDERECO', 50)->nullable();
			$table->string('NUMERO', 20)->nullable();
			$table->string('COMPLEMENTO', 50)->nullable();
			$table->string('BAIRRO', 50)->nullable();
			$table->string('CEP', 9)->nullable();
			$table->string('AGENCIACORRENTE', 15)->nullable();
			$table->string('UF', 40)->nullable();
			$table->string('TIPOTELEFONE1', 30)->nullable();
			$table->string('TELEFONE1', 50)->nullable();
			$table->string('TIPOTELEFONE2', 30)->nullable();
			$table->string('TELEFONE2', 50)->nullable();
			$table->string('CIDADE', 50)->nullable();
			$table->string('TELEFONE3', 50)->nullable();
			$table->string('RECADO', 50)->nullable();
			$table->string('EMAIL1', 50)->nullable();
			$table->string('EMAIL2')->nullable();
			$table->string('CRECI', 20)->nullable();
			$table->char('CORRETOR', 3)->nullable();
			$table->char('INDICADOR', 3)->nullable();
			$table->string('TIPOTELEFONE3', 30)->nullable();
			$table->char('PROMOTOR', 3)->nullable();
			$table->char('CAPTADOR', 3)->nullable();
			$table->float('COMISSAOLOCACAO', 10, 0)->nullable();
			$table->float('COMISSAOVENDA', 10, 0)->nullable();
			$table->char('PLANTONISTA', 3)->nullable();
			$table->float('COMISSAOINDICACAO', 10, 0)->nullable();
			$table->float('VALORAJUDACUSTO', 10, 0)->nullable();
			$table->timestamp('DATAGRAVACAO')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('FILAACESSO', 30)->nullable();
			$table->string('IMOVELACESSO', 50)->nullable();
			$table->string('FOTO')->nullable();
			$table->timestamps();
			$table->float('COMISSAOCAPTACAO', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('corretors');
	}

}
