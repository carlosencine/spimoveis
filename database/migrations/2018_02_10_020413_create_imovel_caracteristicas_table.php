<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImovelCaracteristicasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imovel_caracteristicas', function(Blueprint $table)
		{
			$table->integer('ID', true);
			$table->integer('IMOVEL_ID')->nullable()->index('imovel_caracteristicas_imovel_id_foreign');
			$table->integer('CARACTERISTICA_OPCAO_ID')->nullable()->index('fk_movel_opcaos');
			$table->string('QUANTIDADE', 20)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imovel_caracteristicas');
	}

}
