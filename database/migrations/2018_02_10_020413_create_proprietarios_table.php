<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProprietariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proprietarios', function(Blueprint $table)
		{
			$table->integer('ID', true);
			$table->integer('USUARIO_ID')->nullable();
			$table->char('INATIVO', 3)->nullable();
			$table->char('CE', 3)->nullable();
			$table->char('TFJ', 1)->nullable();
			$table->char('CONFIDENCIAL', 3)->nullable();
			$table->string('NOMERAZAO', 50)->nullable();
			$table->integer('CODIGO')->nullable();
			$table->string('APELIDOFANTASIA', 50)->nullable();
			$table->string('CPFCNPJ', 50)->nullable();
			$table->string('RGIE', 50)->nullable();
			$table->string('INSCMUNICIPAL', 50)->nullable();
			$table->char('SEXO', 1)->nullable();
			$table->text('OBSERVACOES', 65535)->nullable();
			$table->date('DATAJC')->nullable();
			$table->string('REGISTROJC', 30)->nullable();
			$table->date('DATAFUNDACAO')->nullable();
			$table->date('DATANASCIMENTO')->nullable();
			$table->string('NATURALIDADE', 50)->nullable();
			$table->string('NACIONALIDADE', 50)->nullable();
			$table->string('PROFISSAO', 50)->nullable();
			$table->string('ESTADOCIVIL', 50)->nullable();
			$table->char('DEPOSITO', 3)->nullable();
			$table->char('TIPOCONTA', 2)->nullable();
			$table->string('AGENCIACORRENTE', 15)->nullable();
			$table->string('CONTACORRENTE', 30)->nullable();
			$table->integer('CODBANCOCORRENTE')->nullable();
			$table->string('BANCOCORRENTE', 30)->nullable();
			$table->string('FAVORECIDOCORRENTE', 50)->nullable();
			$table->string('CPFCNPJCORRENTE', 50)->nullable();
			$table->string('AGENCIAPOUPANCA', 15)->nullable();
			$table->string('CONTAPOUPANCA', 30)->nullable();
			$table->integer('CODBANCOPOUPANCA')->nullable();
			$table->string('BANCOPOUPANCA', 30)->nullable();
			$table->string('FAVORECIDOPOUPANCA', 50)->nullable();
			$table->string('CPFCNPJPOUPANCA', 50)->nullable();
			$table->string('REGIMEBENS', 50)->nullable();
			$table->string('CERTIDAOCASAMENTO', 50)->nullable();
			$table->string('TIPOENDERECO', 30)->nullable();
			$table->string('ENDERECO', 50)->nullable();
			$table->string('NUMERO', 20)->nullable();
			$table->string('COMPLEMENTO', 50)->nullable();
			$table->string('BAIRRO', 50)->nullable();
			$table->string('CEP', 9)->nullable();
			$table->string('CIDADE', 50)->nullable();
			$table->string('UF', 40)->nullable();
			$table->string('TIPOTELEFONE1', 30)->nullable();
			$table->string('TELEFONE1', 50)->nullable();
			$table->string('TIPOTELEFONE2', 30)->nullable();
			$table->string('TELEFONE2', 50)->nullable();
			$table->string('TIPOTELEFONE3', 30)->nullable();
			$table->string('TELEFONE3', 50)->nullable();
			$table->string('TIPOTELEFONE4', 30)->nullable();
			$table->string('TELEFONE4', 50)->nullable();
			$table->string('SITE')->nullable();
			$table->string('EMAIL1')->nullable();
			$table->string('OPCOES')->nullable();
			$table->string('EMAIL2')->nullable();
			$table->string('RECADO', 50)->nullable();
			$table->string('RECADO2', 50)->nullable();
			$table->string('RECADO4', 50)->nullable();
			$table->string('RECADO3', 50)->nullable();
			$table->float('TAXALOCACAO', 10, 0)->nullable();
			$table->float('TAXAVENDA', 10, 0)->nullable();
			$table->float('TAXA1ALUGUEL', 10, 0)->nullable();
			$table->integer('REPASSE')->nullable();
			$table->string('CONJUGENOME', 50)->nullable();
			$table->string('CONJUGECPF', 50)->nullable();
			$table->string('CONJUGERG', 50)->nullable();
			$table->string('CONJUGENATURALIDADE', 50)->nullable();
			$table->string('CONJUGENACIONALIDADE', 50)->nullable();
			$table->string('CONJUGEPROFISSAO', 50)->nullable();
			$table->date('CONJUGEDATANASC')->nullable();
			$table->dateTime('DATAGRAVACAO')->nullable();
			$table->string('SITESENHA', 40)->nullable();
			$table->integer('REDE_PARCEIRO_ID')->nullable();
			$table->integer('REDE_PARCEIRO_REGISTRO_ID')->nullable();
			$table->integer('REDE_PARCEIRO_REGISTRO_CODIGO')->nullable();
			$table->timestamps();
			$table->char('CONJUNTA', 3)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('proprietarios');
	}

}
