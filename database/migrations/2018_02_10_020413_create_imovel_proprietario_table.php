<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImovelProprietarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imovel_proprietario', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('imovel_id')->unsigned()->nullable();
			$table->integer('proprietario_id')->unsigned()->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imovel_proprietario');
	}

}
