<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToImovelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('imovels', function(Blueprint $table)
		{
			$table->foreign('IMOVEL_SUBTIPO_ID')->references('ID')->on('imovel_subtipos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('imovels', function(Blueprint $table)
		{
			$table->dropForeign('imovels_imovel_subtipo_id_foreign');
		});
	}

}
