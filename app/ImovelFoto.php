<?php

namespace App;

use const false;
use Illuminate\Database\Eloquent\Model;

class ImovelFoto extends Model
{
    protected $table = "imovel_fotos";
    public $timestamps = false;

    protected $fillable = ['imovel_id', 'path', 'description'];

    /*public function imovel()
    {
        return $this->belongsTo(ImovelFoto::class);
    }*/
}
