<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProprietarioResource;
use App\Proprietario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function view;

class ProprietarioController extends Controller
{
    /**
     * Renderiza a view responsável pelo cadastro de proprietários.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewProprietarios()
    {
        return view('proprietarios.main');
    }

    public function add()
    {
        return view('proprietarios.add');
    }

    /**
     * Obtem todos os registros referentes aos proprietários
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $proprietarios = Proprietario::with(['loadImovel' => function ($query) {
            $query->join('imovels', 'imovels.ID', 'imovel_partilhas.IMOVEL_ID');
        }])->get();
            
        return response()->json($proprietarios);
    }

    /**
     * Obtém as informações referentes a um unico proprietário
     *
     * @param $id
     * @return ProprietarioResource
     */
    public function show($id)
    {
        $proprietario = Proprietario::with(['loadImovel' => function ($query) {
            $query->join('imovels', 'imovels.ID', 'imovel_partilhas.IMOVEL_ID');
        }])->where('ID', $id)->get();
        return new ProprietarioResource($proprietario);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Proprietario::create($request->all());
        return response()->json(['message' => 'Proprietário salvo com sucesso!']);
    }

    /**
     * Renderiza a view de exibição de um unico proprietário
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showProprietario($id)
    {
        return view('proprietarios.show');
    }

    /**
     * Renderiza a view de edição de um proprietário
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editInfo($id)
    {
        return view('proprietarios.edit');
    }

    /**
     * Obtém os dados de um proprietario
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProprietarioInfo($id)
    {
        $proprietario = Proprietario::with(['loadImovel' => function ($query) {
            $query->join('imovels', 'imovels.ID', 'imovel_partilhas.IMOVEL_ID');
        }])->where('ID', $id)->get();

        return response()->json($proprietario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Proprietario::where('ID', $id)->update($request->except('load_imovel'));
        return response()->json(['message' => 'Registro atualizado com sucesso']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Proprietario::where('ID', $id)->delete();
        return response()->json(['message' => 'Proprietário removido com suceoo']);
    }

    /**
     * Pega somente o id e o nome de todos os proprietários
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $proprietarios = DB::table('proprietarios')->select('ID', 'NOMERAZAO')->get();
        return response()->json($proprietarios);
    }

    /**
     * Ativa ou desativa o cadastro de um proprietário no sistema
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function statusRecord(Request $request, $id)
    {
        $disable = Proprietario::where('ID', $id);

        $disable->update(['INATIVO' => $request->INATIVO]);
        if ($request->INATIVO == "Sim")
        {
            return response()->json(['message' => 'Registro desativado.']);
        } else if ($request->INATIVO == "Não")
        {
            return response()->json(['message' => 'Registro ativado.']);
        }

    }
}
