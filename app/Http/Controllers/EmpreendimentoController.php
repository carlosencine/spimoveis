<?php

namespace App\Http\Controllers;

use App\Empreendimento;
use Illuminate\Http\Request;

class EmpreendimentoController extends Controller
{
    /**
     * Renderiza a view responsável pelo cadastro de Empreendimentos
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewEmpreendimentos()
    {
        return view('empreendimentos.main');
    }

    /**
     * Obtem a lista de todos os registros cadastrados no sistema
     * com os dados paginados
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $enterprises = Empreendimento::paginate(50);
        return response()->json($enterprises);
    }

    /**
     * Obtem a lista de todos os registros cadastrados sem paginação
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $all = Empreendimento::all();
        return response()->json($all);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Empreendimento::create($request->all());
        return response()->json(['message' => 'Empreendimento adicionado com sucesso.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empreendimento  $empreendimento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Empreendimento::where('ID', $id)->update($request->all());
        return response()->json(['message' => 'Registro atualizado com sucesso.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empreendimento  $empreendimento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Empreendimento::where('ID', $id)->delete();
        return response()->json(['message' => 'Registro excluido com sucesso.']);
    }
}
