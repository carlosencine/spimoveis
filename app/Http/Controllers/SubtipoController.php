<?php

namespace App\Http\Controllers;

use App\Subtipo;
use Illuminate\Http\Request;

class SubtipoController extends Controller
{
    public function viewCategoria()
    {
        return view('categorias.main');
    }

    public function categorias()
    {
        $subtipos = Subtipo::all();
        return response()->json($subtipos);
    }

    public function store(Request $request)
    {
        Subtipo::create($request->all());
        return response()->json(['msg' => 'Categoria adicionada com sucesso!']);
    }

}
