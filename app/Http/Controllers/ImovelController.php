<?php

namespace App\Http\Controllers;

use App\Http\Resources\ImovelCollection;
use App\Http\Resources\ImovelResource;
use App\Imovel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ImovelController extends Controller
{

    /**
     * Renderiza a listagem dos imóveis
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewImovel()
    {
        return view('imoveis.main');
    }

    /**
     * Renderiza a view para cadastrar novo Imóvel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('imoveis.add');
    }

    public function showImovel($id)
    {
        return view('imoveis.show');
    }

    /**
     * Retorna os dados vindos da API
     * @return ImovelCollection
     */
    public function index()
    {
        $imoveis = Imovel::with(['subtipos', 'loadProprietario' => function ($query) {
            $query->join('proprietarios', 'proprietarios.ID', 'imovel_partilhas.PROPRIETARIO_ID');
        }])->get();
        return new ImovelCollection($imoveis);
    }
    /**
     * @param $id
     * @return ImovelResource
     */
    public function show($id)
    {
        /*$imovel = Imovel::with(['subtipos', 'loadProprietario' => function ($query) {
            $query->join('proprietarios', 'proprietarios.ID', 'imovel_partilhas.PROPRIETARIO_ID')->select(['IMOVEL_ID', 'PROPRIETARIO_ID', 'PORCENTAGEM',
                'TAXALOCACAO', 'TAXAVENDA', 'REPASSE']);
        }])->where('ID', $id)->get();*/

        return new ImovelResource(Imovel::find($id));
    }

    /**
     * Salva o imóvel no banco
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $imovel = new Imovel();

        $res = $imovel->create($data);

        for ($i = 0; $i < count($data['rows']); $i++) {
            $data['rows'][$i]['IMOVEL_ID'] = $res->id;
        }

        for ($c = 0; $c < count($data['CARACTERISTICAS']); $c++) {
            $data['CARACTERISTICAS'][$c]['IMOVEL_ID'] = $res->id;
        }

        $imovel->proprietario()->attach($data['rows']);
        $imovel->caracteristica()->attach($data['CARACTERISTICAS']);

        return response()->json(['status' => 'Salvo com sucesso!']);
    }

    /**
     * Pega as informações de um determinado imóvel para o formulário de exibição de dados
     *
     * @Param $id
     * @return \Illuminate\Http\JsonResponse
     * */
    public function showImovelInfo($id) {
        $admin = Imovel::with(['subtipos', 'loadProprietario' => function ($query) {
            $query->join('proprietarios', 'proprietarios.ID', 'imovel_partilhas.PROPRIETARIO_ID')->select(['IMOVEL_ID', 'PROPRIETARIO_ID', 'PORCENTAGEM',
                'TAXALOCACAO', 'TAXAVENDA', 'REPASSE']);
        }])->where('ID', $id)->get();

        return response()->json($admin);
    }

    /**
     * Obtem as caracteristicas de um imóvel para exibir nos formulários de exibição/edição
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCaracteristicas($id) {
        $caracteristicas = DB::table('imovel_caracteristicas')->select(['IMOVEL_ID', 'CARACTERISTICA_OPCAO_ID', 'QUANTIDADE'])
            ->where('IMOVEL_ID', $id)
            ->get();
        return response()->json($caracteristicas);
    }

    /**
     * Renderiza o formulário de edição de um registro
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {
        return view('imoveis.edit');
    }

    /**
     * Pega as informações de um registro para o formulário de edição.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getImovel($id) {
        $imovel = Imovel::with(['subtipos', 'caracteristica', 'loadProprietario' => function ($query) {
            $query->join('proprietarios', 'proprietarios.ID', 'imovel_partilhas.PROPRIETARIO_ID')->select(['IMOVEL_ID', 'PROPRIETARIO_ID', 'PORCENTAGEM']);
        }])->where('ID', $id)->get();

        //$imovelImages = $this->fotos->where('imovel_id', $imovel->CODIGO);

        return response()->json($imovel);
    }

    public function loadAdminInfo() {
        $proprietarios = DB::table('imovel_partilhas')->where('IMOVEL_ID', $id)
            ->get();
        return response()->json($proprietarios);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id) {
        $data = $request->except(['load_proprietario', 'rows', 'CARACTERISTICAS']);

        DB::table('imovels')->where('ID', $id)->update($data);

        //Verificar se o campo updateDescription é diferente de vazio em caso negativo efetuar o update do
        //respectivo campo na tabela

        //Atualiza as caracteristicas
        $data_caracteristicas = $request->input('CARACTERISTICAS');

        for ($i = 0; $i < count($data_caracteristicas); $i++) {
            $data_caracteristicas[$i]['IMOVEL_ID'] = $id;
        }

        DB::table('imovel_caracteristicas')->select('CARACTERISTICA_OPCAO_ID')->where('IMOVEL_ID', $id)
            ->delete();
        DB::table('imovel_caracteristicas')->insert($data_caracteristicas);

        //Atualiza os proprietários
        $data_proprietarios = $request->input('load_proprietario');

        for ($i = 0; $i < count($data_proprietarios); $i++) {
            $data_proprietarios[$i]['IMOVEL_ID'] = $id;
        }

        DB::table('imovel_partilhas')->select('PROPRIETARIO_ID')->where('IMOVEL_ID', '=', $id)->delete();
        DB::table('imovel_partilhas')->insert($data_proprietarios);

        //Retorna a mensagem para a view
        return response()->json(['message' => 'Imóvel atualizado!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Imovel::where('ID', $id)->delete();
        return response()->json(['message' => 'Imóvel excluido!']);
    }

    /**
     * Ativa ou desativa um registro no banco
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function statusRecord(Request $request, $id) {
        $disable = Imovel::where('ID', $id);

        $disable->update(['INATIVO' => $request->INATIVO]);
        if ($request->INATIVO == "Sim") {
            return response()->json(['message' => 'Registro desativado.']);
        } else if ($request->INATIVO == "Não") {
            return response()->json(['message' => 'Registro ativado.']);
        }

    }
}
