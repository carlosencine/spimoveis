<?php

namespace App\Http\Controllers;

use App\Corretor;
use Illuminate\Http\Request;

class CorretorController extends Controller
{
    /**
     * Renderiza a view responsável pelo cadastro de corretores
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewCorretor()
    {
        return view('corretores.main');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $corretores = Corretor::paginate(50);
        return response()->json($corretores);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Corretor::create($request->all());
        return response()->json(['message' => 'Corretor salvo com sucesso']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Corretor::where('ID', $id)->update($request->all());
        return response()->json(['message' => 'Dados atualizados com sucesso!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Corretor::where('ID', $id)->delete();
        return response()->json(['message' => 'Corretor excluido com sucesso']);
    }
}
