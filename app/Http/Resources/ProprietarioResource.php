<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProprietarioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
        /*return [
            'ID' => $this->ID,
            'USUARIO_ID' => $this->USUARIO_ID,
            'INATIVO' => $this->INATIVO,
            'CE' => $this->CE,
            'TFJ' => $this->TFJ,
            'CONFIDENCIAL' => $this->CONFIDENCIAL,
            'NOMERAZAO' => $this->NOMERAZAO,
            'CODIGO' => $this->CODIGO,
            'REGISTROJC' => $this->REGISTROJC,
            'DATAJC' => $this->DATAJC,
            'DATAFUNDACAO' => $this->DATAFUNDACAO,
            'TIPOCONTA' => $this->TIPOCONTA,
            'APELIDOFANTASIA' => $this->APELIDOFANTASIA,
            'CPFCNPJ' => $this->CPFCNPJ,
            'RGIE' => $this->RGIE,
            'INSCMUNICIPAL' => $this->INSCMUNICIPAL,
            'SEXO' => $this->SEXO,
            'DATANASCIMENTO' => $this->DATANASCIMENTO,
            'NATURALIDADE' => $this->NATURALIDADE,
            'NACIONALIDADE' => $this->NACIONALIDADE,
            'PROFISSAO' => $this->PROFISSAO,
            'ESTADOCIVIL' => $this->ESTADOCIVIL,
            'DEPOSITO' => $this->DEPOSITO,
            'AGENCIACORRENTE' => $this->AGENCIACORRENTE,
            'CONTACORRENTE' => $this->CONTACORRENTE,
            'CODBANCOCORRENTE' => $this->CODBANCOCORRENTE,
            'BANCOCORRENTE' => $this->BANCOCORRENTE,
            'FAVORECIDOCORRENTE' => $this->FAVORECIDOCORRENTE,
            'CPFCNPJCORRENTE' => $this->CPFCNPJCORRENTE,
            'AGENCIAPOUPANCA' => $this->AGENCIAPOUPANCA,
            'CONTAPOUPANCA' => $this->CONTAPOUPANCA,
            'CODBANCOPOUPANCA' => $this->CODBANCOPOUPANCA,
            'BANCOPOUPANCA' => $this->BANCOPOUPANCA,
            'FAVORECIDOPOUPANCA' => $this->FAVORECIDOPOUPANCA,
            'CPFCNPJPOUPANCA' => $this->CPFCNPJPOUPANCA,
            'REGIMEBENS' => $this->REGIMEBENS,
            'CERTIDAOCASAMENTO' => $this->CERTIDAOCASAMENTO,
            'ENDERECO' => $this->ENDERECO,
            'NUMERO' => $this->NUMERO,
            'COMPLEMENTO' => $this->COMPLEMENTO,
            'BAIRRO' => $this->BAIRRO,
            'CEP' => $this->CEP,
            'CIDADE' => $this->CIDADE,
            'UF' => $this->UF,
            'TIPOTELEFONE1' => $this->TIPOTELEFONE1,
            'TELEFONE1' => $this->TELEFONE1,
            'TIPOTELEFONE2' => $this->TIPOTELEFONE2,
            'TELEFONE2' => $this->TELEFONE2,
            'TIPOTELEFONE3' => $this->TIPOTELEFONE3,
            'TELEFONE3' => $this->TELEFONE3,
            'TIPOTELEFONE4' => $this->TIPOTELEFONE4,
            'TELEFONE4' => $this->TELEFONE4,
            'EMAIL1' => $this->EMAIL1,
            'EMAIL2' => $this->EMAIL2,
            'RECADO' => $this->RECADO,
            'RECADO' => $this->RECADO2,
            'RECADO' => $this->RECADO3,
            'RECADO' => $this->RECADO4,
            'TAXALOCACAO' => $this->TAXALOCACAO,
            'TAXAVENDA' => $this->TAXAVENDA,
            'TAXA1ALUGUEL' => $this->TAXA1ALUGUEL,
            'REPASSE' => $this->REÁSSE,
            'CONJUGENOME' => $this->CONJUGENOME,
            'CONJUGECPF' => $this->CONJUGECPF,
            'CONJUGERG' => $this->CONJUGERG,
            'CONJUGENATURALIDADE' => $this->CONJUGENATURALIDADE,
            'CONJUGENACIONALIDADE' => $this->CONJUGENACIONALIDADE,
            'CONJUGEPROFISSAO' => $this->CONJUGEPROFISSAO,
            'CONJUGEDATANASC' => $this->CONJUGEDATANASC,
            'load_imovel' => $this->loadImovel,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];*/
    }
}
