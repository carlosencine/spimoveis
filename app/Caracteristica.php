<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caracteristica extends Model
{
    protected $table = 'imovel_caracteristica_opcaos';
    public $timestamps = false;

    public function imovel()
    {
        return $this->belongsToMany('App\Imovel');
    }
}
