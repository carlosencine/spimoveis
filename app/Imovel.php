<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imovel extends Model
{
    //protected $table = 'imovels';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
        'OUTRO_ID',
        'EMPREENDIMENTO_ID',
        'IMOVEL_SUBTIPO_ID',
        'CODIGO',
        'CODIGOANUNCIO',
        'CHAVESENTREGA',
        'INATIVO',
        'OPCAOSF',
        'OCUPADO',
        'FIADOR',
        'DEPOSITO',
        'PLACA',
        'DATACAPTACAO',
        'CHAVESQUADRO',
        'TIPOENDERECO',
        'ENDERECO',
        'NUMERO',
        'COMPLEMENTO',
        'CEP',
        'BAIRRO',
        'CIDADE',
        'UF',
        'GUIA',
        'TIPO',
        'RELOGIOLUZ',
        'RELOGIOAGUA',
        'VISITA',
        'DORMITORIO',
        'BANHEIRO',
        'SUITE',
        'TERRENOLADOESQUERDO',
        'TERRENOLADODIREITO',
        'CONSTRUIDA',
        'VAGA',
        'IPTU',
        'MATRICULA',
        'VALORVENDA',
        'VALORLOCACAO',
        'VALORCONDOMINIO',
        'VENDA',
        'LOCACAO',
        'TERRENOTOTALM2',
        'STATUS',
        'HORARIOVISITA',
        'DESCRICAO',
        'CONDOMINIOAGUA',
        'CONDOMINIOLUZ',
        'CONDOMINIOGAS',
        'CONDOMINIOTVCABO',
        'CONDOMINIOIPTU',
        'TERRENOFRENTE',
        'TERRENOFUNDOS',
        'TERRENORAIO',
        'LATITUDE',
        'LONGITUDE',
        'ALTITUDE',
        'DATAFINALCONTRATO',
        'VALORLOCACAOM2',
        'VALORVENDAM2',
        'EXCLUSIVIDADE',
        'FRENTEPARA',
        'FUNDOSPARA',
        'ESQUERDAPARA',
        'DIREITAPARA',
        'CARTORIODATAREGISTRO',
        'CARTORIOFOLHA',
        'CARTORIOREGISTRO',
        'CARTORIOLIVRO',
        'CARTORIO',
        'TIPOZONA',
        'DESTAQUE',
        'COMPLEMENTOANDAR',
        'COMPLEMENTOAPTO',
        'DESCRICAOSITE',
        'LANCAMENTO',
        'TEMPORADA',
        'VALORIPTU',
        'DIVULGARVALOR',
        'PARAMETRIZACAO_ID',
        'VALORVENDACUSTO',
        'AREAUTIL',
        'SITEDIVULGAR',
        'SITE_VISITAS',
        'PAIS',
    ];

    public function subtipos()
    {
        return $this->belongsTo('App\Subtipo', 'IMOVEL_SUBTIPO_ID', 'ID');
    }

    public function proprietario()
    {
        return $this->belongsToMany('App\Proprietario', 'imovel_partilhas', 'IMOVEL_ID', 'PROPRIETARIO_ID')
            ->withPivot('PORCENTAGEM', 'IMOVEL_ID');
    }

    public function loadProprietario()
    {
        return $this->hasMany(Imovel_partilha::class,'IMOVEL_ID', 'ID');
    }

    public function caracteristica()
    {
        return $this->belongsToMany('App\Caracteristica', 'imovel_caracteristicas', 'IMOVEL_ID','CARACTERISTICA_OPCAO_ID')
            ->withPivot('QUANTIDADE', 'CARACTERISTICA_OPCAO_ID');
    }

    /*public function fotos()
    {
        return $this->hasMany(ImovelFoto::class, 'imovel_id', 'CODIGO');
    }*/
}
