<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subtipo extends Model
{
    protected $table = 'imovel_subtipos';
    public $timestamps = false;
}
