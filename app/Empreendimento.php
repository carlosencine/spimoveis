<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empreendimento extends Model
{
    public $timestamps = false;
    protected $fillable = ['NOME', 'ZELADOR', 'PORTEIRO', 'TELEFONE1'];
}
