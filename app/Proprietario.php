<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proprietario extends Model
{
    protected $dates = ['DATAGRAVACAO'];

    protected $fillable = [
        'ID',
        'USUARIO_ID',
        'INATIVO',
        'CE',
        'TFJ',
        'CONFIDENCIAL',
        'NOMERAZAO',
        'CODIGO',
        'REGISTROJC',
        'DATAJC',
        'DATAFUNDACAO',
        'TIPOCONTA',
        'APELIDOFANTASIA',
        'CPFCNPJ',
        'RGIE',
        'INSCMUNICIPAL',
        'SEXO',
        'DATANASCIMENTO',
        'NATURALIDADE',
        'NACIONALIDADE',
        'PROFISSAO',
        'ESTADOCIVIL',
        'DEPOSITO',
        'AGENCIACORRENTE',
        'CONTACORRENTE',
        'CODBANCOCORRENTE',
        'BANCOCORRENTE',
        'FAVORECIDOCORRENTE',
        'CPFCNPJCORRENTE',
        'AGENCIAPOUPANCA',
        'CONTAPOUPANCA',
        'CODBANCOPOUPANCA',
        'BANCOPOUPANCA',
        'FAVORECIDOPOUPANCA',
        'CPFCNPJPOUPANCA',
        'REGIMEBENS',
        'CERTIDAOCASAMENTO',
        'ENDERECO',
        'NUMERO',
        'COMPLEMENTO',
        'BAIRRO',
        'CEP',
        'CIDADE',
        'UF',
        'TIPOTELEFONE1',
        'TELEFONE1',
        'TIPOTELEFONE2',
        'TELEFONE2',
        'TIPOTELEFONE3',
        'TELEFONE3',
        'EMAIL1',
        'EMAIL2',
        'RECADO',
        'TAXALOCACAO',
        'TAXAVENDA',
        'TAXA1ALUGUEL',
        'REPASSE',
        'CONJUGENOME',
        'CONJUGECPF',
        'CONJUGERG',
        'CONJUGENATURALIDADE',
        'CONJUGENACIONALIDADE',
        'CONJUGEPROFISSAO',
        'CONJUGEDATANASC',
    ];

    public function loadImovel()
    {
        return $this->hasMany(Imovel_partilha::class, 'PROPRIETARIO_ID','ID');
    }
}
