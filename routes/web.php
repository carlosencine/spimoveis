<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('imoveis.main');
});*/

Route::prefix('admin')->group(function () {
    Route::get('imoveis', 'ImovelController@viewImovel');
    Route::get('imoveis/add', 'ImovelController@add');
    Route::get('imoveis/{id}', 'ImovelController@showImovel');
    Route::get('imoveis/edit/{id}', 'ImovelController@edit');

    Route::get('proprietarios', 'ProprietarioController@viewProprietarios');
    Route::get('proprietarios/add', 'ProprietarioController@add');
    Route::get('proprietarios/show/{id}', 'ProprietarioController@showProprietario');
    Route::get('proprietarios/edit/{id}', 'ProprietarioController@editInfo');

    Route::get('categorias', 'SubtipoController@viewCategoria');
});
