<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request)
{
    return $request->user();
});

/*Imóveis*/
Route::get('/imoveis', 'ImovelController@index');
Route::post('/imoveis', 'ImovelController@store');
Route::get('/imoveis/show/{id}', 'ImovelController@show');
Route::put('/imoveis/{id}', 'ImovelController@update');
Route::get('/imoveis/info/{id}', 'ImovelController@getImovel');
Route::put('/imoveis/status/{imovel}', 'ImovelController@statusRecord');
Route::delete('/imoveis/{id}', 'ImovelController@destroy');

/*Categorias*/
Route::get('/categorias', 'SubtipoController@categorias');
Route::post('/categorias', 'SubtipoController@store');

/*Caracteristicas*/
Route::get('/caracteristicas', 'CaracteristicaController@index');
Route::get('/caracteristicas/info/{id}', 'CaracteristicaController@getCaracteristicas');

/*Proprietarios*/
Route::get('/proprietarios', 'ProprietarioController@index');
Route::get('/proprietarios/info/{id}', 'ProprietarioController@show');
Route::put('/proprietarios/{id}', 'ProprietarioController@update');

/*Empreendimwntos*/
Route::get('/empreendimentos', 'EmpreendimentoController@index');

/*Fotos*/
Route::get('/fotos/{id}', 'ImovelFotoController@show');
Route::post('/fotos/upload', 'ImovelFotoController@store');
Route::put('/fotos/upload/{upload}', 'ImovelFotoController@update');
Route::delete('/fotos/upload/{upload}', 'ImovelFotoController@destroy');
Route::put('/fotos/upload/changeorder/{upload}', 'ImovelFotoController@changeOrder');